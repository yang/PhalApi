<?php

interface Core_DB
{
	public function connect();
	
	public function disconnect();
}