<?php
/**
 * Core_Exception 自定义异常类
 *
 * - 对于系统已存在Exception类，故加Zen前缀以区分为自定义的类
 *
 * @author: dogstar 2014-10-02
 */

class Core_Exception extends Exception
{
	
}
