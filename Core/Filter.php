<?php
/**
 * 拦截器接口
 *
 * @dogstar 2014-10-25
 */

interface Core_Filter
{
    public function check();
}
