<?php

return array(
    /** ---------------------- framework translation - developer ---------------------- **/
    'service ({service}) illegal' => 'service ({service}) illegal',
    'no such service as {className}' => 'no such service as {className}',
    'mcrypt_module_open with {cipher}' => 'mcrypt_module_open with {cipher}',
    'No table map config for {tableName}' => 'No table map config for {tableName}',
    'Call to undefined method Core_DI::{name}() .' => 'Call to undefined method Core_DI::{name}() .',
    "miss {name}'s enum range" => "miss {name}'s enum range",
    '{name} should be in {range}, but now {name} = {value}' => '{name} should be in {range}, but now {name} = {value}',
    "min should <= max, but now {name} min = {min} and max = {max}" => 'min should <= max, but now {name} min = {min} and max = {max}',
    '{name} should >= {min}, but now {name} = {value}' => '{name} should >= {min}, but now {name} = {value}',
    'miss name for rule' => 'miss name for rule',
    '{name} require, but miss' => '{name} require, but miss',
    'Core_Api::${name} undefined' => 'Core_Api::${name} undefined',
    'Bad Request: {message}' => 'Bad Request: {message}',
    'Interal Server Error: {message}' => 'Interal Server Error: {message}',

    /** ---------------------- application translation - product ---------------------- **/
	'Hello {name}, Welcome to use PhalApi!' => 'Hello {name}, Welcome to use PhalApi!',
    'user not exists' => 'user not exists',
);
